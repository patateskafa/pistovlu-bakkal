__[Piştovlu Bakkal](https://pistovlubakkal.com)__ kaynak kodudur.

### [Stack](https://www.haskellstack.org/) ile statik içeriği oluşturmak için:
```shell
$ git clone https://github.com/patateskafa/pistovlu-bakkal.git
$ cd pistovlu-bakkal
$ stack clean && stack build && stack exec site rebuild && stack exec site watch
```

ardından _[http://localhost:8000](http://localhost:8000)_'den görüntülenebilir

[Hakyll](https://jaspervdj.be/hakyll) ile üretilmiştir, daha fazla bilgi
için dokümanları okunabilir. Kaynak kod [özgürce](http://ozgurlisanslar.org.tr/gpl/gpl-v3/) kurcalanabilir,
değiştirilebilir, yeniden dağıtılabilir. Yazı içerikleri kaynak
gösterilerek kullanılabilir.
