---
title: İletişim
---

Eski ve güzel [e-posta](mailto:alp@pistovlubakkal.com)  ile bir tık uzağınızdayım.

For non-Turkish speakers: I can be contacted via the good ol' [e-mail](mailto:alp@pistovlubakkal.com).