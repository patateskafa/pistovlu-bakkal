---
author: Alp
title: Siftah
tags: bakkal, blog, sosyal medya
teaser: "Ezcümle, siz zaten daha ilk satırdan fark etmişsinizdir, buranın bir
olayı yok. Öğrenirken heyecan duyduğum şeyleri paylaşabileceğim, derli
toplu ya da dağınık fikirlerimi toparlayabileceğim, bohem ihtiyacımın
bir kısmını karşılayabileceğim, uçucu olmayan bir mecra. Eski ve güzel,
bilindik bir blog denemesi"
---

Bu bloga girişmeme iki temel itki sebep oldu. İlki hello
vörld. Yani merhaba dünya, kanalıma hoş geldin. Yani bu:

```java
public static void main(String[] args) {

    System.out.println("Merhaba dünya");

}

```

Hikaye olağan. Ortadirek ailenin Y kuşağına mensupluktan kelli
bilgisayar eve küçük yaşta girdi. Yıllardır tuşluyorum bu mereti ve
affedersiniz hısım akraba arasında hep söylenegelirdi _"Alp
bilgisayardan anlar"_ diye. Oysa anlamıyordum, genelde oyun
oynuyordum. Ortaokul metalikacısı olduğum vakitlerde anneme arkadaşının
_"Oğlan bunu okusun, gelecek burada."_ diye verdiği Memik Yanık'ın tuğla
C++ kitabı yıllarca aynı masanın aynı köşesinde aynı el değmemişlikle
durdu. Bir defa açıp da bakmadı neymiş diye, kafasız. Sonra liseler
üniversiteler işler güçlerden geçildi, tesadüfler birbirini kovaladı ve
hayat; yukarıdaki kod bloğunu bir metin editörüne yazıp, derleyip
koşturmama ancak üç yıl evvel vesile olabildi. Hiç olmayabilirdi de;
fakat iyi ki oldu. Bu tam teşekküllü program ekrana _"Merhaba dünya"_
yazdırıyordu, tek işi buydu -çünkü ondan daha fazlasını istememiştik!-,
işi bittiği gibi de süreci sonlandırıyordu, vurucu bir şarkının sözleri
gibiydi, sesler değişiyor renkler değişiyordu, yutuptaki kır saçlı
Amerikalı _"Tebrikler, artık sen de bir programcısın!"_ diye tazyiği
veriyordu, etme eyleme ağabey, yanaklar kızarıyordu...

Yıllardır tuşladığım şeyin aslında ne olduğunu yeminler olsun ki o
dandik ve harikulade programı çalıştırdığım an anladım; dışarıdan görene
yirmi altı, içeriden bakana altı yaşımdaydım. Anlamanın coşkusu, ne
kadar da muhteşem bir his.

İkinci itkiyi Twitter kullanıcılığım süresince keşfettim: Beni
bağlayacak, üzerinden yargılanabileceğim, kantarlara vurulabileceğim
sözleri söylemenin bir aracına olan ihtiyaç. Fakat anlık haber takibi
haricinde, derli toplu sözler söylemek için tivitır bok gibi bir mecra
ve bu başka bir hikaye. İnsanlığın mutlaka ve mutlaka masaya ciddi
biçimde yatıracağı ve genelgeçer ölçütler belirleyeceği "sağlıklı ve
sorumlu sosyal medya kullanıcılığı"nın ve "sağlıklı ve sorumlu sosyal
medya kullanıcısı" karakterinin üzerine düşünmeyi ve bu örnek karakteri
yaratmaya çalışmayı çok anlamlı buluyorum; fakat bu da başka ve uzun bir
hikaye, Bakkal'da (hehehe bakınız hemen "Bakkal" oluverdi, bohem
şelalesi) bilahare tartışmaya çalışacağım. Dağıtmayayım.

Dağıtmayayım ama ağızdan sosyal medya bir defa çıktı, bu noktada bir
dakika durup internet ve web dediğimiz, insanlığın göz alıcı tarihi
boyunca ürettiği en muhteşem icatlardan bu ikisine dikkatinizi kısa
süreliğine rica ediyorum. Ciddiyim. Bunu etraflıca anlatmadan bu
dükkanın varlık sebebini açıklayamam. Bildirimler fırtınasında
yaşadığımız için farkına varmamız fiilen imkansızlaşıyor; fakat yine de
durup feysbuku, istekramı vb. tamamen unutup bir anlığına doksanlardaki
o büyük patlamanın arifesine, 56K modemlerin hüküm sürdüğü evrene ve o
evrenin tanrılarına dönelim: Bakınız... Bu blogu yazıyorum, yayımlıyorum
ve internet denen deli kablolarına fiziki erişimi olup web'e de dahil
olmuş herhangi bir insan bu yazıya dandik bir link aracılığıyla anında
ulaşabiliyor! __BU MUHTEŞEM BİR ŞEY DEĞİL Mİ?__ Buradaki ya da _Merhaba
Dünya_ programındaki sade ihtişamı -yeniden- keşfetmenin; insanlığın
teknolojiyle kurduğu/kuracağı -ruhen ve bedenen- sağlıklı ilişkinin
tesisi konusunda çok ufuk açıcı olduğunu düşünüyorum. Bu nedenle
Bakkal'ın mühendislik açısından hiçbir iddia taşımayan [kaynak
kodu](https://gitlab.com/patateskafa/pistovlu-bakkal) tamamen
[özgürdür](http://ozgurlisanslar.org.tr/gpl/gpl-v3/), veresiyedir.
Bakkal'da sizi izleyen gugul analitiği ve benzeri bir sinsi ayak izi
avcısı yoktur; sağında solunda gerilmeden gezebilir, "bilgisayar"a hem
yabancılaşmamak (yani kendimizi gerçekleştirmemize yarayan değil,
sömüren bir unsur olarak karşımızda dikilmemesi) hem de yabanileşmemek
(yani new-age bir pre-dijitalcilik özlemine kapılmamak), ona tarihsel
hakkını vermek, eğlenmek (bilgisayar kullanıcılığı çok eğlenceli bir
şey, bunu da kendimce açmaya çalışacağım), muhteşem olanaklarından nasıl
faydalanacağımızı kurgulamak üzere derin tefekkürlere
dalabilirsiniz. Bunu çok isterim.

Ezcümle, siz zaten daha ilk satırdan fark etmişsinizdir, buranın bir
olayı yok. Öğrenirken heyecan duyduğum şeyleri paylaşabileceğim, derli
toplu ya da dağınık fikirlerimi toparlayabileceğim, bohem ihtiyacımın
bir kısmını karşılayabileceğim, uçucu olmayan bir mecra. Eski ve güzel,
bilindik bir blog denemesi.

Peki neden bakkal? Çünkü çeşidi bol, miktarı naif tutacağız; tutmaya
çalışacağız. Bilgisayardan hâlâ anlamıyorum; ancak anlamaya çalışıyorum
ve bu çok keyifli bir şey. Bu veletçe keyfi yaymaya çalışacağım. Bloga
eşlik eden düşük yoğunluklu bir [yutup
kanalı](https://www.youtube.com/channel/UCbKLgt-blkbZ0kMldsOgyaQ) da
olacak. Fakat ne blogda ne de kanalda aman aman bir içerik trafiği
olamayacak, hayat o kadarına izin vermiyor. Ürün yelpazesinin kalanına
da keyfimin kâhyası karar verecek; siyaset, kent, müzik, sonra biraz o,
biraz şu; fakat neticede hepinizi çaktırmadan hak yol Emacs dinine
döndüreceğim, esas gizli ajanda bu! Oh be, blog varmış. __: )__

Ha, neden piştovlu? Çünkü sinekli değil. Çünkü Halide Edip kuşkusuz
bizdendir; ama değildir de. Piştovlu Bakkal salt dükkanımız değil
muhidimiz, mahallemizdir de; öyle olmaya çalışır, öyle olmasına
çalışırız. Burada külahlı tipleri, tarihi ittirenleri, doğumları
zorlayanları severiz.

Mümkün olduğunca sık görüşmek üzere.
