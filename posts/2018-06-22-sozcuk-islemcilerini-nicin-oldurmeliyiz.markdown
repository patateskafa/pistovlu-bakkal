---
author: Alp
title: Sözcük işlemcilerini niçin öldürmeliyiz?
tags: yutup, vim, emacs
teaser: "Meseleyi anlatmamı kolaylaştırması için kanaldaki videoları bu yazıyla
paslaştıracağım. Vim nasıl yüklenir, Emacs'in neresinden tutalım, bunun
rengini nerden ayarlıyorduk, yav bizimkisi hata veriyor vb. hepsi anneye
anlatım şefkatiyle bu yazıda"
---

Bloga eşlik edecek yutup kanalının ilk içeriklerini üretmeye
başlıyorum. Başlıktan -haklı olarak- bir şey anlamayanlar [serinin giriş videosuna](https://www.youtube.com/watch?v=V0RYGNT85eI)
hızlıca göz atabilirler.

Meseleyi anlatmamı kolaylaştırması için kanaldaki [videoları](https://www.youtube.com/watch?v=V0RYGNT85eI&list=PLqmgTsn4_VjRYhDdw5vF9u5pfTCLReVMg) bu
yazıyla paslaştıracağım. Vim nasıl yüklenir, Emacs'in neresinden
tutalım, bunun rengini nerden ayarlıyorduk, yav bizimkisi hata veriyor
vb. hepsi anneye anlatım şefkatiyle bu yazıda... Takıldığınız, çalışmayan,
etmeyen, aklınızı kurcalayan her meselede bir [e-mail](mailto:alp@pistovlubakkal.com)
uzağınızdayım.

## Vim
### Windows
Windows'ta Vim'i çalıştırabilmemizin birkaç yolu var,
bunlardan en kolayları:

#### En iyi yol
Windows'u silip ya da yanına yer açıp bir GNU/Linux dağıtımı edinmek :)
Tamam tamam, geçiyoruz.

#### gVim
gVim yani "graphical Vim", Vim'i komut satırı arayüzü yerine bağımsız
bir grafik arayüzünde çalıştırmamızı sağlayan bir program. Vim'in resmi
[sitesinden](https://www.vim.org/download.php#pc) tak diye -gvimXX.exe-
indirip, şak diye çalıştırabilirsiniz!

![(gVim manzarası)](/images/2018-06-22/vim1.png "gVim")

#### vim-plug kurulumu
- Windows tuşuna basalım, ya da fare ile başlat menüsünü açalım. "powershell" yazıp ilgili programı bulmasını bekleyelim.

- ![(Başlat menüsünde PowerShell'i buluyoruz)](/images/2018-06-22/vimpsh0.png "Başlat menüsünde PowerShell'i buluyoruz")

- PowerShell'e sağ tıklayıp "Yönetici olarak çalıştır/Run as administrator" seçelim ve karşımıza yeni bir PowerShell penceresi gelsin
```bash
md ~\vimfiles\autoload
$uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
(New-Object Net.WebClient).DownloadFile($uri,$ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath("~\vimfiles\autoload\plug.vim"))
```
- Komutlarını sırasıyla satır satır kopyalayıp PowerShell'e yapıştırıp entera basıyoruz
- Not: "cd" komutundan sonra tırnak işaretlerinin arasına _:echo $VIMRUNTIME_ komutunun sonucunu yazmalısınız
- Komutlarını kopyalayıp PowerShell'e yapıştırıp entera basalım. (Komutları elinizle yazmayın, kopyalayıp yapıştırın. Yapıştırma işlemini mouse sağ tuşu ile yapabilirsiniz)
- Bu noktada Vim ana dizini altındaki vimfiles klasöründe yeni bir autoload dizini oluşmuş, vim-plug bu dizine indirilmiş ve çalışmaya hazır hale gelmiş oluyor.
- vim-plug hazır, [videoya](https://youtu.be/xdOBUdDfx_Q) dönebilirsiniz.

#### Linux için Windows Alt-sistemi (Windows Subsystem for Linux)
Hayın Canonical (Ubuntu'yu geliştiren şirket) ve Microsoft işbirliği ile
Windows 10'un falanca sürümünden itibaren içinde gelen bir Linux
alt-sistemi var; fakat kullanabilmek için önce aktifleştirmemiz
gerekiyor.

Sırasıyla:

- Başlat menüsünü açıp "Geliştirici ayarları" yazıp karşımıza gelen
menüye giriyoruz (Windows'u İngilizce kullanıyorsak "Use developer
features" yazabiliriz)
- Radyo tuşlarından "Geliştirici modu"nu seçiyoruz ("Developer mode")
- Yine başlat menüsünü açıp "Windows özelliklerini aç veya kapat"
   yazıyoruz ve karşımıza gelen menüyü seçiyoruz ("Turn Windows features
   on and off")
- Listeden "Linux (Beta) için Windows Alt sistem"in yanındaki kutuya
   tik atıveriyoruz ve "Tamam"a basıyoruz ("Windows Subsystem for
   Linux")
- Linux alt sistemimiz hazır. İstediğimiz zaman başlat menüsüne girip
   "bash" yazarak karşımıza çıkan terminal emülatörünü seçebilir ve
   _bash_ maceramıza başlayabiliriz.
- Muhatap olduğumuz şey tam anlamıyla bir alt-sistem, yani mevcut
   Windows kurulumunuz ile paylaştığı neredeyse hiçbir şey yok. Bu da
   demek oluyor ki, _bash_ aracılığıyla bir dosya yaratırsanız bu
   dosyaya yine ancak _bash_ komut satırı ile ulaşabilirsiniz. Bir de
   başlangıçta bir parola belirlemenizi isteyecek, bu _root_ yani kök
   kullanıcı parolamız; unutmuyoruz, bir köşeye not ediyoruz.
- Şimdi alt sistemimize Vim'i yükleyelim, "$" karakterinden sonraki
   komutu _bashe_ yazıyoruz ve entera basıyoruz

```bash
pattizwin@DESKTOP-ZZZZZZ:~$ sudo apt-get install -y vim
```
- _bash_ bizden kök kullanıcı parolamızı girmemizi isteyecektir,
   giriyoruz ve yine entera basıyoruz.
- Biraz bekliyoruz.
- Vim'iniz hazır, [videoya](https://www.youtube.com/watch?v=tvRnKOA3sQs) dönebilirsiniz.

![(Ubuntu Bash manzarası)](/images/2018-06-22/vim2.png "Ubuntu Bash")

### macOS
- Hemen bir [terminal açıyoruz](https://duckduckgo.com/?q=macos+terminal+nasıl+açılır&t=h_)

```bash
havali-mekbukum:~ kullanici$ brew install vim
```
- Biraz bekliyoruz
- Vim'iniz hazır, [videoya](https://www.youtube.com/watch?v=tvRnKOA3sQs) dönebilirsiniz.

### Platform nedir bilmeyenler
Vim [özgür ve açık
kaynaklı](https://tr.wikipedia.org/wiki/Özgür_ve_açık_kaynak_kodlu_yazılım)
bir yazılım olduğu için platform fark etmeksizin cepte duran bir seçenek
de kaynak kodunu indirip derlemek.

Kaynak kodu ve -maalesef İngilizce- derleme/çalıştırma yönergelerine
[buradan](https://github.com/vim/vim) ulaşabilirsiniz.

## Emacs
Emacs'i [indirme sayfası](https://www.gnu.org/software/emacs/download.html)na gidip ilgili yönergeleri izleyerek işletim sistemimize kolayca yükleyebiliriz.
