---
author: Alp
title: Yeni bir dünyanın eşiğinde öncülük - I - Giriş
tags: siyaset, kuşlar, öncülük
teaser: "Peki nedir bunca lakırtımın sebebi? Yeni bir dünyanın eşiğinde öncüleşme
sancısının tespit ettiğim farklı ideolojik, siyasi, pratik veçhelerini
ve aynı anda hem kuş hem insan olmaya çalışmanın bireyde yarattığı kimi
gerilimleri, blog çerçevesinin sunduğu akademik olmama özgürlüğünü de
kullanarak tartışmaya çalışmak istiyorum"
---

### Yeni bir dünyanın eşiğinde öncülük - I - Giriş
İnsan, günler ve geceler boyunca ufka her baktığında, bilinemezlerle
dolu bir karanlığı görüyorsa eğer, ürkmeye başlar. Ve, başlarda,
mutlaka, ertesi günlere umutla uyanır "Bugünler de geçecek!" Aslında
birçok açıdan basit canlılarız, iyi anlamda. Ne kadar büyük badirelerden
çıkmış olursa olsun, biraz huzura kavuşunca, hayatı olumlayabilmek
kolaydır insan için.

Maddeyi ve zamanı, iç içe geçen bu iki biricik hakikati, eğip
bükemiyoruz, onları kandıramıyoruz, yalnızca anlamaya çalışabiliyoruz,
çünkü müdahalenin ön koşulu anlamaktır. Bilinçlerde madde ve zamanın
akışının soyutlamaları belirir, akıl bunları bir denkleme oturtur, aklın
yarattığı maddi araçlarla madde ve zamana, yine madde ve zamanın
sınırlılıkları içinde kalarak müdahale eder insan. Nehir akmaya devam
ediyordur, soyutlanacak başka bir madde ve zaman oluşur, ve yine
usavurum, ve yine müdahale, ve yine nehir akar...

Sürekli değişen maddeyi ve zamanı, akan nehirleri baştan ve baştan,
farklı ölçeklerde (birey, kişisel çember, mahalle, okul, iş yeri, kent,
ülke, bölge, dünya, uzay...) soyutlama, soyutladıkça müdahale etme ve
yine maddece ve zamanca belirlenmenin iç içe geçmiş sonsuz döngüsü. Yani
büyük konuşmayalım ama, diyalektik. Her insan, adını öyle koymasa dahi,
ömrünü bu diyalektik devinim içinde farklı düzeylerde ve ölçeklerde hem
belirleyen hem de belirlenen olarak geçirir.

Fizikçiler, felsefeciler ve bu kavramları dert edinmiş olası diğer çürük
dirseklilerinden dayak yemeden önce konumuza gelelim: sıradan insanlar
ve kuşlar.

Kuşların işi hep kolaydı. Yeterli bir irtifadan toplumlara
baktıklarından kelli, tablo onlar için, tarih boyunca apaçık
olagelmiştir: Sıradan insan, zaman aktıkça, madde devindikçe, ne
yapıyorsa yapsın, ufuktaki karanlıklar dağılmıyorsa, hatta her şey daha
kötüye gidiyorsa eğer, köklerinden başlayarak kurumaya başlar önce.

Yavaş bir süreçtir, kovuğuna dalına yaprağına çiçeğine çabuk yansımaz,
hemen fark edilmez. Günler, haftalar, aylar ve yıllar boyunca, yorgun
gözler yeni güne ve geceye, hep bir öncekinden daha zayıf umutlarla
açılır. Umut etmek eylemi, kabul etmesi zor olsa da, giderek bir
kandırmacaya dönüşüyordur.

İşte maddeyi ve zamanı kandıramayan, müdahalesi de fayda etmeyen sıradan
insan; toprağının yarınını, sevdiklerinin, çocukların ve doğmamış
çocukların yarınını görünmez kılan koyu karanlığın ve huzursuz
bilinemezliğin kıvamında bir seyrelme sezemiyorsa, bir zaman sonra
doğabilecek bir güneşin ufacık bir ipucunu seçemiyorsa gözleri, verdiği
basit sözleri dahi tutamamanın yükü altında giderek daha çok eziliyorsa,
yetersizlik ve başaramama hissi benliğini sarmışsa, yeni bir güne veya
geceye uyanmak istemiyorsa; onu vaktiyle hayata ve toprağına bağlayan,
fakat kuruyan kökleri bir bir kopmaya başlar.

Kuruyan kökler koptukça, kanarlar, ve bu süreçte açığa çıkan enerji,
toplumun isyan kazanında birikmeye başlar. Zor alışılır bir acı ve
feryat yükselir her seferinde, dil dudak ısırılır. Kökler derindedir,
nasıl alışılsın? Bizim insanımız, özellikle, gururludur, çehresine
bakınca kanadığı anlaşılsın da istemez, dilini dudağını ısırması
bundandır. İnsanımız benim, insanımız sizsiniz. İnsanımız ailemiz,
eşimiz, arkadaşımız, az görebildiğimiz can dostumuz, her gün gördüğünüz
komşumuzdur ve elbette, hiç tanımadığınız milyonlarca tanıdık
yabancıdır.

İnsan dişini sıkar da sıkar, kendi küçük hayatında elinden geleni yapar
vaziyeti düzeltmek için. Fakat, her şey daha kötüye gidiyorsa hâlâ;
günlerin ve gecelerin sonunda, yalnızca ve yalnızca maddenin ve zamanın
hükmü geçer. Gövdesi kurumaya yüz tutar bir zaman sonra. Yavaş yavaş
kurur, belki yine yıllar alır, kurudukça yine canı acır, sessiz sessiz
feryat eder. Yaşam, yapraklarından da gider ve meyveleri dalında çürüyüp
düşmeye başlar sonunda.

Ve yurttaş ağacı, kalan son birkaç zayıf köküyle, ölmeye yüz tutmuştur
artık. Gövdesi henüz devrilmemişken dahi, türlü canavarlığa meyyal bir
hayvan meyvelenmeye başlar kütüğünün ortasında. Yalnızca temele
indirgenmiş içgüdüleriyle yaşayan; toplumdan, birlik olmaktan, kolektif
mutluluktan ve hüzünden, parçası olduğu dokudan tamamen kopmuş bir
canavar hücresi, ve çoğalıyor, kuruyan ağacı içten içe çürütüyor ve
sarıyor.

Bu bozulma, toplum ormanımızın görece ufak bölgeleriyle, veya az sayıda
yurttaşla sınırlı kalsaydı, nitelik eşiğini aşamasaydı, halk kendi
kendini sağaltabilirdi belki. Fakat vaziyet böyle değil. Duyuyoruz ki,
yurttaşlarımız, hep bir ağızdan, giderek daha gür feryat
ediyorlar. Toplumun isyan kazanında biriken enerji, patlamaya yetecek
bir basınca varmanın eşiğindedir.

Köklerimizden kopuyor, feryat ediyor, alıştıkça hissizleşiyor,
canavarlaşıyor ve çürüyoruz. Güzel ve zor memleketimizde (ve dünyanın
diğer güzel ve zor memleketlerinde) kitlesel mülksüzleşmenin, adalet
hissinin yitiminin, milyonlarca göze aynı anda görünen bitimsiz kesif
karanlığın; geniş kitlelerde ve aydınlarda farklı çıktıları olsa da, bu
halk katmanlarının tümü için benzer kuruma, kopuş ve canavarlaşma
mekanizmaları çalışıyor derinlerde. Kuşlar bu süreçlere, tarih boyunca,
farklı coğrafyalarda, defalarca kere şahit oldular.

Bu kitlesel yıkımların tarihsel olarak sonuncusu, neredeyse elli yıla
yayılmıştır ve neoliberalizmin eseridir. Bugün, bildiğimiz anlamda
neoliberalizmin sonuna geldik. Hakim sınıfların uluslararası ve yerel
klikleri başka kurguları hayata geçirmeye uğraşıyorlar, emperyalizm kriz
yaşıyor, güçler dengesine ağırlık koyan yeni aktörler yükseliyor, grev
dalgaları, işçi ve köylü eylemleri kabarıyor, toplumlar göçe zorlanıyor,
savaşlar farklı coğrafyalarda sürüyor, premodern akımlar iktidara
geliyor veya kitleler içinde kök salıyor, vesaire... Güzel ve zor
ülkemizde de, dünya genelinde de, toplumların isyan kazanlarında elli
yıllık yıkımdan kaynaklanan bir enerji birikimi var. Bu birikim yavaş
yavaş farklı kanallara akmaya da başladı. Yeni bir dünyanın eşiğindeyiz,
kuşlar bunu görüyorlar.

Diyalektik her insanın farkında olsun veya olmasın, işlettiği bir
süreçtir, diye bir iddia attık ortaya. Geniş kitleler asla aptal
değildir, yalnızca zaman ve madde algıları sınırlıdır, sınırlı
bırakılmıştır. Kendi sınırlılıkları içinde oldukça zekidirler aksine,
onlara küçümseyerek yaklaşan burnu havada ukalaları suya götürüp susuz
getirecek bir kıvraklıktadır zekaları. Fakat tarih bilinçleri,
uzgörüleri, soyutlama kabiliyetleri limitlidir. Hayat olağan bir biçimde
akıyorken pek çok şeyi sezebilir, fakat büyük ve köktenci dönüşümleri
tasavvur edemez, bu dönüşümlerin gerektirdiklerini bilince
çıkaramazlar. Ve bu normaldir, maddenin doğası gereği bu böyledir.

Bu noktada öncülük devreye girer. Öncüler, daha güçlü bir uzgörüyle,
soyutlama ve müdahale yeteneğiyle, ait oldukları sınıfın çıkarları,
ideolojik konumlanışları ve bunların çıktısı olan siyasi kurgularıyla
biriken enerjiden pay almaya ve onu kendi manivelaları yapmaya
çalışırlar. Öncülerin madde ve zaman algıları, uzgörüleri ve müdahale
kabiliyetleri de başka değişkenlerce sınırlanır. Öncülüğün de kademeleri
vardır. Fakat kademe ve ideoloji farkı gözetmeksizin, öncülüğün ilk
gereği, kuşlaşmaktır, insan benliğinden sıyrılıp toplumun maddesine
belirli bir irtifadan bakabilmektir. Öncülüğün ikinci gereği ise yere
inebilmek, irtifadan görünen tablodan yapılan çıkarımlarla bir müdahale
tasarısını, insan benliğinde hayata geçirmektir.

Burjuva akımlarını bir kenara bırakalım, meselemiz sosyalizm. Sosyalist
öncü, toplumun kazanına akan enerjiyi patlamadan evvel okuyabilen bir
halk çocuğu, güç damarlarını keşfetmeye çalışan bir bilimci, kazmayı
doğru yere vurmaya çalışan bir madenci, fışkıran cevhere doğru şekli
vermeye çalışan bir zanaatkardır. Bu topraklarda doğduk. Yurttaş
ormanımız kuruyorken, toplumun isyan kazanındaki birikimi, halkımızın
yüzündeki, sesindeki incelikleri en iyi bu toprakların devrimcileri
kavrayabilir ve müdahale araçlarını yaratabilir. Kazanda biriken enerji
düzen içi kanallara burjuva öncülerce akıtıldığında, hatta müsilaj gibi
bir anda yüzeye vuran bir canavarlık dalgası -faşizm- ortaya çıktığında,
iş işten geçmiştir. İş işten geçebilir de, madde devinmeye devam
edecektir ve başka olanaklar, bir zaman sonra belirecektir. Yıkım ve
acıyla dolu uzun bir dönem daha göze alınıyorsa elbette...

Edebiyat parçalamış bulundum, fakat yanlış anlaşılmasın, öncülük derken
bir tür süper kahramanlıktan bahsetmiyorum elbette, aksine, sosyalistler
için öncüleşmenin öznesi sıradan insandır. Öncüleşme, insan olmanın
getirdiği bütün hata paylarını da içeren bir evrim sürecidir. Tekrar
etmek pahasına, yerden havalanıp, çeşitli irtifalardan topluma,
insanların kendilerini göremediği geniş açılardan bakabilme ve yere
konup topluma müdahale edebilme, bunun araçlarını yaratabilme yetisini
kazanmaktır. Kuşlardan rol çalınan bu süreç, beraberinde pek çok
sorumluluğu ve kimi gerilimleri getirir.

En baştan söylemeli, kadercilik ve nihilizm simsarı değiliz,
bilimciyiz. Asla değişmeyecek tükenmişliklere bakıp bakıp aynı mavalı
okuyan kör papağanlar değil, ufuktaki aydınlıkların ipuçlarının
madencileriyiz. İnsandan umudu kesen, kesmeyi öğütleyen sollu sağlı
postmodern ibişler gibi, karanlık gidişatın fotoğrafını çekip
bırakmayacağız meselenin ucunu. Hem halkımıza borçluyuz, hem de kendi
yaşamımızı anlamlandırmakta can suyumuzdur bu çaba. Bahsini ettiğimiz
zorlu ve çürütücü süreç, değiştirilemez bir yazgı değil asla. Tam
aksine, insanımız, tıpkı tarihte yaptığı gibi, mutlaka ve mutlaka
dağıtacaktır bu kesif karanlığı, bundan kuşku duymuyorum.

Peki nedir bunca lakırtımın sebebi? Yeni bir dünyanın eşiğinde öncüleşme
sancısının tespit ettiğim farklı ideolojik, siyasi, pratik veçhelerini
ve aynı anda hem kuş hem insan olmaya çalışmanın bireyde yarattığı kimi
gerilimleri, blog çerçevesinin sunduğu akademik olmama özgürlüğünü de
kullanarak tartışmaya çalışmak istiyorum. Kendi zihnimde bugüne dek inşa
ettiğim fikir duvarının tuğla dizilimini paylaşmak, tartışmak,
eksiklerimi görerek ve katkı alarak duvarımı daha da sağlamlaştırmak
gibi bir amacım var.

Yazı konuları ve sırası biraz rastgele olacak, fakat bir bütünlüğü
korumaya çalışacağım, aşağıda üzerine yazmayı düşündüğüm kimi konu
başlıklarını ilgilisi için paylaştım. Bunlar değişebilir, araya başka
unsurlar girebilir. Oldukça yoğun ve dağınık çalıştığım, hayatımı
şimdilik ancak bu şekilde idame ettirebildiğim için yazı aralıkları
düzensiz olacak, fakat arayı çok açmamaya çalışacağım.

Diyebilirsiniz ki, yav düdük, bunlara dair kalem oynatma ehliyetin var
mı? Bilemiyorum, umarım anlamlı bir çabadır. Fakat haddimi bilerek
yazmaya özen göstereceğim, yine de sürçülisan edersem kusuruma
bakmayın. Bir de, toplumumuzun isyan kazanı o kadar dolu ki, belki ben
bu yazı dizisini tamamlayamadan, bağrından çıkaracağı öncülerle memleket
bambaşka noktalara evrilir, çabam boşa düşer, iyi ki de düşer. Yine de,
o vakte kadar, deneyeceğim. Çok uzattım. Değerli vaktiniz için teşekkür
ederim.

Sevgiler.



_Kimi konular:_

- Tarihsel maddecilik
  - Bilimsel yöntemi benimsemek zorunda mıyız?
  - Bilim ve bilimsel yöntem kuşatılmıştır
  - Tarihselci olmak zorunda mıyız?
  - Maddeci olmak zorunda mıyız?
  - Tarihsel maddeci olmak zorunda mıyız?
- Sınıflar mücadelesi
  - Burjuvazi ve kendi içindeki saflaşmalar
  - Emperyalizm ve dünyada güçler dengesi
  - Çin Halk Cumhuriyeti ve uygarlığın yeni ufukları
  - Emekçi halk ve tabakaları - Mavi yaka, beyaz yaka, küçük burjuvazi,
    köylülük, işsizler ve lümpenproletarya
  - Dinin ve milliyetçiliğin tarihselliği
  - "Orta sınıf" meselesi ve kolay erişilebilir meyveler
- İktidarı neden istiyoruz?
  - İktidar lanetli bir şey mi?
  - İktidar her yerde mi?
  - Güç yozlaştırır mı?
  - Devlet, bürokrasi, para, piyasalar bizimle ne kadar yaşayacak?
- Öncü - kitle diyalektiği
  - Öncü neden öncüdür? Öncülük doktriner bir günahsızlık vaizliği midir?
  - Öncülük tipleri ve kademeleri
  - Kitle neden kitledir? Halk aptal mıdır?
  - Öncü ve kitle arasındaki geçişkenlik havuzu
  - İki sapma: Elitizm ve halklaşmak
  - "Ortak aklımız parti" aşılabilmiş midir?
  - Öncünün alet çantası: Parti, program, strateji, model, taktik,
    ittifaklar ve popülizm
  - Hülyalılık - gerçekçilik diyalektiği
  - Aydın, işlevi ve önücünün görevini aydına havale etmek
  - Halk tabakalarına nasıl yaklaşılır, yahut A.C.A.B. saçmalığı
  - Web çağında yeni olanaklar
- Öncü bilinç ve duygulanım
  - Web çağında duygudurum kontrolü
  - Bir "iyi yurttaşlık" kıstası olarak sorumlu web kullanıcılığı
  - Kentli okumuşun hapishanesi: _Kaygı - Nihilizm - Hedonizm_ fasit
    çemberi ve meyvesi, dengine yönelen öfke ve yanlış bilinç: Sinizm
- Cumhuriyet devrimimiz
  - Mustafa Suphiler bizdendir, fakat bizi doğuran Cumhuriyettir
  - Devrimler neden zorunlu olarak kendi toprağında biter? Veya "neden
    komşunun devrimini yapamayız?"
- Bir tasarım nesnesi ve zanaat olarak siyaset
  - Kısıtlı kaynaklar, büyük hülyalar ve omlet tarifleri
  - Siyaset alfabesinin iki harfi: Vurgu ve sessizlik
  - Kalın ve ince çizgi diyalektiği
  - İdeoloji, akademi ve siyaset: Farkları nedir?
  - Siyasette dört işlem ve bakkal hesabı
  - Siyasetin ağırlık merkezleri
- Postmodern kardeşler: Kimlikçi sol ve neofaşizm
  - Postmodern sol kentli okumuşu ve aydını nasıl esir aldı?
  - Postmodern sağ kitlelerin birikmiş öfkesini nereye kanalize ediyor?
  - Postmodern sol ve sağın ortak ideolojik kökleri
  - Kabalaşmak pahasına, kimi kuramsal fonksiyonların türevleri\
    _f(a) = a + 2_ ise\
    _frankfurt'(q) = ? | foucault'(x) = ? | arendt'(y) = ? | popper'(z) = ?_
  - Postmodernizm sanatı nasıl sepetleştirdi?
- Kentli emekçinin iki yaşam motifi: Kentlilik ve kasabalılık
  - Köyden kente göç: Emekçi kuşakların bayrak yarışı
  - Kentlilik bugün neden "kent soyluluk"tan farklıdır ve ilericidir?
  - Kentlilik burjuva zevkler veya snobluk mudur?
  - Kasabalılık neden gericidir?
