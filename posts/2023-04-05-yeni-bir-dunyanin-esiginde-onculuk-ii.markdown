---
author: Alp
title: Yeni bir dünyanın eşiğinde öncülük - II - Madde ve tarihin maddesi
tags: madde, sancı, lezzet
teaser: "Fizik laboratuvarında, işimiz görece kolaydı. Peki tarihin ve
toplumların maddesiyle uğraşırken 'maddi gerçek' tespitini nasıl
yapacağız, nasıl bir deney düzeneği kuracağız?"
---

### Yeni bir dünyanın eşiğinde öncülük - II - Madde ve tarihin maddesi
#### Maddeyle muhabbet
Son bir yıldır, işten güçten kafayı kaldıramamaktan dolayı, insanlığın
maddeyle olan muhabbetine biraz uzak düştüm. Bu yazıyı yazmadan evvel,
"Maddede ne durumdayız yav, rezil olmayalım?" diye Aspect, Clauser,
Zeilinger üçlüsüne Nobel kazandırdan deneylere dair yazılıp çizilenlere
son bir defa baktım ve emin oldum: Öncelikle, insanlık doğanın
yasalarını bilimsel yöntemle keşfetmeye devam ediyor, maddenin kuantum
düzeyindeki davranış yasalarının önemli bir unsurunu, Kuantum
Dolanıklığı, deney düzeyinde kesin biçimde kanıtlamakla kritik bir
mesafeyi katettik. Bu yasaların makro düzeyle kurduğu ilişkiyi de bir
gün mutlaka açıklayacağız... Bütün bu keşif süreci, tek kelimeyle,
büyüleyici!  Madde, bu esrarengiz şey, her bir unsuruyla, ve yine,
aldığı her formda, büyüleyici. Madde ve büyü, ironik ama, öyle.

Din tacirleri, kuantum tokatçıları ve bilumum şarlatanlığın esnafını
üzmek pahasına, doğanın yasalarını keşfe çıktığımız binlerce yıllık
yolculuğumuzda; tanrı, ruh, sihir, öcü yahut böcünün izine rastlamadık
şimdiye dek. Ben kendi koşturmacama dalmış, uzak kalırken bu
gelişmelere, aksine delalet edecek bir keşfi de olmamış insanlığın. İçim
rahatlamadı diyemem, aksi durumda temelinden sarsılırdı fikir duvarımız,
keza bilim affetmez. Maddenin bilinmeyen coğrafyalarında, bildiğimiz bir
metodolojiyle, bilimsel yöntemle, yol almaya devam ediyoruz.

Doğa yasalarının keşfi cephesinden son haberleri, bilim okuryazarı
düzeyinde alıp, artık içim de rahat olduğundan kelli, epistemolojide ve
ontolojide Politzer Baba çizgisi dahi bu yazı bağlamında anlatmaya
çalıştıklarım için yeterli olacaktır diye düşünüyorum.

Temel bilimlerin laboratuvarında ürettiğimiz bilgiye sırtımızı yaslamak,
görece konforludur. Gökyüzünün rengine dair iki farklı argüman varsa,
bunlardan maddenin ve doğanın yasalarını gözeteni, genellikle,
argümanların öne sürüldükleri bağlamda, gökyüzünün rengini daha iyi ve
kapsamlı bir biçimde açıklıyordur.

-- *Gökyüzü mavi, çünkü allah öyle yaratmış...*\
-- *Hayır, gökyüzü mavi, çünkü mavi ışınların dalga boyları...*

Veya:

-- *Gökyüzü kapkara, çünkü kalbim kırık, gökyüzü ruhumun kuytuları kadar
  kara...*\
-- *Hayır, gökyüzü mavi, çünkü kuramsal fizik henüz edebiyatı
  açıklamıyor...*
  
Güldürüden hallice, basit bir gerçeğe işaret ederek devam edelim: Hem
fizik biliminin nesnesi olan madde için, hem de tarihin ve toplumların
"maddesi" için meselenin başlangıç noktası gerçekten de
aynıdır. Toplumlar, fiziksel açıdan, maddedir! Bu, kulağa çok komik
gelse de, bence önemli ve temel bir mantık basamağı. Fiziğin inceleme
nesnesi olan maddeden oluşan bu canlılar bütünü, bağrında herhangi bir
ruh, sihir, doğaüstü güç barındırmaz. Fiziğin belki de üst soyutlamaları
diyebileceğimiz kimya ve biyoloji de, bildiğimiz üzere, maddenin
canlılık sürecinde nasıl dönüştüğünü ve geliştiğini, evrimleştiğini
açıklamaya çalışır.

İnsan insan olana, topluluklar ve toplumlar oluşturmaya başlayana dek,
onun maddesine dair ürettiğimiz bilgi, temelde bu süreçlere dairdir ve
bunlarla sınırlıdır. "Evrim var ama onu da Allah yarattı" argüman
ailesini şimdilik dışarıda tutuyorum, bence siyasileşen bireyin
ideolojik evrimi bağlamında değerli bir tartışma, fakat şimdilik tutarlı
bir maddecilik duvarı inşa etmeye çalışmakla yetinelim, bunları ve
benzerlerini ileride tartışacağız.

İnsan, evrim serüveni boyunca, hayvanlar aleminin diğer türlerinden,
doğayı ve kendi doğasını dışsal birer unsur olarak kavrayarak
ayrışabildiği (doğaya ve kendisine kuş bakışı bakabildiği), doğaya bu
yeni bilinç düzeyiyle şekil vermeye ve topluluklar halinde yaşamaya
başladığı noktadan itibaren, fiziğin kimyanın ve biyolojinin
laboratuvarına çevrili merceğimizin ince ayarını yapıp, tarihi ve
toplumları görüş alanımıza almamız gerekir.

Hemen sonrasında, artık kadrajı bu yeni düzeyi görecek biçimde
sabitlenmiş olan merceğin ayar kolunu bırakıp, zaman makinemize
yönelebiliriz. Taş Devri'nde durmakta olan zaman ibresini hızla günümüze
çekersek ve atıyorum, merceği son bir hareketle Ankara'da rastgele bir
sokağa odaklarsak, gördüğümüz ilk renk, gridir. Hikayesi yarım kalmış
güzel Ankaramızın grisi değildir bu. Sokaktaki yurttaşlar kendilerine
uzatılan mikrofona şöyle demektedirler:

Muhabir: *Oyunuz kime?*\
Yurttaş 1: *Tayyip Erdoğan'a*\
Muhabir: *Neden?*\
Yurttaş 1: ***E nedeni var mı, her şey ortada!***\

Muhabir: *Oyunuz kime?*\
Yurttaş 2: *Kılıçdaroğlu'na*\
Muhabir: *Neden?*\
Yurttaş 2: ***E nedeni var mı, her şey ortada!***

Vay lele vay! Aynı maddi gerçekliğe doğrultulmuş, dil düzeyinde sesteş,
anlam düzeyinde bambaşka iki önerme. Toplumun maddesinin; sınfılar
mücadelesinin, siyasetin, kültürün, ideolojilerin ve türlü toplumsal
yapı ve motifin grilerle, amorf biçimlerle dolu karmakarışık
düzlemindeyizdir artık. Tarihin ve toplumların gelişim yasalarını
anlamaya çalışıyoruzdur ve az evvel fizik laboratuvarında hissettiğimiz
konfordan eser kalmamıştır.

#### Maddeciyiz ezelden
Ürkütücü gri karmaşıklığa odaklanmadan evvel, en sade anlamıyla
maddeciliğe dair konuşalım biraz. Kafayı taktığımız kimi tarihsel
kesitleri incelemeye başladığımızda, ya da bugün başımızı çevirip etrafa
baktığımızda, az evvelki güldürünün devamı olabilecek denli basit bir
olgu daha çıkar karşımıza: Tarih boyunca insanlar, tartışmaya yer
bırakmayacak düzeyde maddeci olagelmişlerdir.

Burada maddecilik derken, hayatla kurulan temel bir ilişkilenme
biçiminden bahsediyoruz. İnsanlar, insan toplulukları ve toplumlar,
tarihin belirli bir anında, fikren ne kadar idealizm/metafizik
belirlenimli olurlarsa olsunlar, günün sonunda maddi olgulara bakıp
kendi hayatlarına, ailelerine, karınlarını nasıl doyuracaklarına, av
takvimlerine, hasatlara ve bağ bozumlarına, işlerine, siyasi
hareketlere, üretime ve bölüşüme, hizmetlere erişime, savaş meydanlarına
veya barış masalarına dair tasarruflarda bulunmuşlar, kararlar
vermişlerdir. Maddi gerçeklerden kaçmaya çalışanlar ise, bu duruma
kendilerini zihinlerinde nasıl ikna etmiş olurlarsa olsunlar, yine maddi
gerçeklerce belirlenmişlerdir.

Tarih öncesinde ve Antik Çağ'da, atıyorum hava koşullarından, arazi
eğiminden, savaş teçhizatından, savaşçılarının morallerinden yola
çıkarak değil de falanca ruha, efsuna, tanrıya güvenerek cenk eden
topluluklar olmuşsa da, olgulara dayanarak hareket eden daha maddeci
hasımlarınca alaşağı edilişleri muhtemelen çok hazin olmuştur. Ya da
örneğin, tersinden diyebiliriz ki, Grigori Rasputin'in çarlık
hanedanının bir kısmı üzerinde tesirini artırabilmesi, siyasi ve
ekonomik olarak kıskaçta olan Çarlık Rusya'nın maddi gerçeğe takla
attıramayacağını anladığı zamanlarda, köhnemiş aristokrasinin daldığı
ruhsal buhranların sonucu gerçekleşebilmiştir.

Çağımızın siyaset kurumuna bakalım, kulağa maddeciliğe en yabancı gelen
unsurlara örneğin. Mesela "Muhafazakarlık", modern bir kategoridir,
siyasal söyleminin ve programının bir kısmını kadim addettiği kimi
motifleri, eskinin masalsı asr-ı saadetlerinin cilalanmış imajlarını,
modern zamanlarda muhafaza etmek üzerine kurar, fakat günün sonunda
bütün siyasalarının özünü maddenin durumuna, olgulara göre belirler. Bu
maddeciliğin ete kemiğe bürünüşüne şahit olmak için uzaklara bakmaya
gerek yok, MSP geleneğinden günümüz AKP'sine, DEVA'sına kısa bir bakış
atmak dahi yeter. Keza, günümüzün popüler kavramı "Siyasal İslam", yine
MSPgillerin dinci İslamcı ideolojisinin maddenin sınırlılıklarında
uygulayabildiği, modern bir iktidar stratejisinin adıdır. Premodern bir
uhreviyatın modern siyasette kendini maddenin sınırlılıkları içinde var
edişidir. Keza IŞİD, Taliban, Mormonlar, o bu şu...

"Temel bir yaşam motifi olarak maddecilik"ten türeyen örnekleri
çeşitlendirebiliriz. Rastgele bir başka alanı ele alalım, örneğin,
modern psikoterapi, bireyin maddeyle, yaşamındaki olgularla olan
ilişkisini onarmak üzerine kurulu bir akademi ve pratiktir... Örnekler
çoğaltılabilir. Kısacası, maddi dünyanın olguları, tarihte bireyler ve
toplumlar açısından mutlak belirleyici olmuştur diyebiliriz. Geri kalan
her şey, bilinçlerde ya da kolektif bilinçlerde nasıl bir görüngüye
sahip olursa olsun, maddeden kök alır ve belirlenendir.

Yani, belli bir bilişsel düzeyin üzerine çıkılamayan çocukluk gibi
dönemlerin veya edebiyat, şiir, sanat dalları vb. soyutluklarla uğraşan
yaratıcı edimlerin dışında, hayatla temel ilişkilenim düzeyinde maddi
gerçekten kaçmak, ekseriyetle ya bir bilmezliğin, ya bir çaresizliğin,
ya da farklı düzeylerde patolojilerin eseridir ve genellikle, toplumsal
ya da bireysel düzlemde, bir tür yıkımla sonuçlanır.

#### Kaçan konfor
Fizik laboratuvarında, işimiz görece kolaydı. Peki tarihin ve
toplumların maddesiyle uğraşırken "maddi gerçek" tespitini nasıl
yapacağız, nasıl bir deney düzeneği kuracağız? Burada, fizik
laboratuvarına kıyasla konforumuzu kaçıran esas unsur, tespit
sürecimizin ürününün, yani kuramın, maddi gerçeğin bir kısmını öyle veya
böyle ıskalayacak olması zorunluluğudur. Soyutlamanın doğasının
gereğidir bu. Fizik deneylerinin çıktıları nettir, tarih ve toplum
deneylerinin çıktıları, çok daha bulanıktır. Yani kabaca, tarihin
kantarında tarttığımız kuramımız/soyutlamamız, tarihin maddesine kıyasla
belirli ölçülerde hafif gelecektir, bazı şeyleri açıklamakta, her zaman
yetersiz kalacaktır. "Hayır, öyle bir soyutlama yapacağım, öyle bir
kuram kuracağım ki, her şeyi açıklayacak!" Bunu yapmak için akışın, yani
evrenin ve zamanın kendisi, bir tür tanrı olmak gerekir ve bu,
imkansızdır. Edebiyatı güzeldir, o ayrı.

Neyse, diyelim ki, güç bela başladık soyutlamaya... Şu herhalde şundan
böyle, bu sanırım şundan şöyle, çünkü... Tam bu noktada,
bilinemezciliğin ve hiççiliğin çekici konforu etrafımızı sarmış,
grilerin diyarında tartışıyorken, işimiz yeterince zor değilmiş gibi,
ortaokul sıralarından gelen bir soru da mutlaka yükseliverir bir
yerlerden: *"Eee, kime göre neye göre?"* İlk gençlikte ufuk açıcı
olabilecek bu soru, bir paket French Theory ve bir miktar mısır
nişastası ile koyulaştırılıp fırına verildiğinde, meşhur postmodern
amentü *"Her şey her şey olabilir"*e dönüşür. Tarihin inkarının en veciz
sloganlarındandır.

Usavurum sürecinde tarihsel ve toplumsal gerçeğin bir kısmını ıskalamak
zorunluluğu, tarihin ve toplumların mekanizmalarını yeter düzeyde
açıklanamaz mı kılmaktadır? Beraberinde bir tür bilinemezciliği veya
hiççiliği zorunlu olarak getirmekte midir?

Hayır. Çok katmanlı, çok değişkenli, karmakarışık denklemler olsalar
dahi, iddiamız, tarihin ve toplumların değişim, dönüşüm ve gelişim
yasalarını yeter düzeyde tespit edebileceğimizdir. Bu son derece zorlu
ve konforsuz bir çaba olsa da. Buradaki "yeter düzeyi", 11. Tez'in
sınırı belirler, buna geleceğiz. Her şey her şey olamaz. Her şey her şey
olabilircilik, alt metninde "tarih yoktur, varsa da onun mekanizmalarını
kavramamız imkansızdır" der. Kuramsal düzeyde bir tür hiççilik, özne
düzeyinde ise iddiasızlıktır, kaybetmeye ve başka güçlerce belirlenmeye
mahkumiyettir. Bunu önücülüğün farklı irtifalarını; akademi, ideoloji ve
siyaset ayrımını, postmodern sol ve sağ kuzenleri tartışırken daha
detaylıca açmaya çalışacağım.

#### Anlamak tercihinin sancısı ve tarihin lezzeti
Bu noktada bir öncüleşme sancısı tespit edebiliriz diye
düşünüyorum. Okumaya, veri toplamaya ve akıl yürütmeye başlayan öncü
bilinç, sorgulamalarla dolu yollar yürüdükten sonra, bir fikir kavşağına
varır. Bir yönde maddenin zenginliği üzerinde yükselen tarihin ve
toplumların maddesini kavramaya çalışmak çabası uzanır, en zorlu ve
sancılı yol budur, diğerlerinde ise, maddenin belirleyiciliğini veya
tarih mefhumunu öyle veya böyle yadsıyan yahut tersine çeviren renk renk
idealizm, metafizik, bilinemezcilik, hiççilik, boşvermişlik. Yapılacak
olan, oldukça bireysel ve iradi bir seçimdir, ideolojiyi belirler.

Hayatın, ülkenin, toplum olarak içine düştüğümüz çukurun yarattığı bütün
iç sıkıntımız, öfkemiz ve coşkumuzla tarihsel maddeciliğin patikasını
seçtik diyelim. Anlamak ve değiştirebilmek için, bu yoldan yürümeye
niyetimiz var. Tarihin ve toplumların mekanizmaları diye bir şey var ve
biz ona vakıf olmaya çalışıyoruz. Ne bekliyor bizi o yolda? Ne
yapacağız?

Maddenin ve zamanın devinimini durduramıyoruz diye sıkça tekrar
ettim. Gerçekten de, maddenin tarih ve toplumlar düzeyinde aldığı
biçimleri incelerken, elimizden gelen ve bütün çabamız, bu sürekli akış
içinde belirli uğraklarda durup, durduğumuz o uğraklardan toplumlara,
yapılara ve öznelere attığımız bakışlarla kimi soyutlamalara varmak, bu
soyutlamaların farklı ölçeklerde sürekliliklerini ve kopuş noktalarını
tanımlamak işidir aslında. Peki kulağa korkutucu ve hatta ağdalı gelen,
hiçbir reçetesi olmayan, başı sonu gözükmeyen işi nasıl olacak da
becerebileceğiz? Becerebilecek miyiz? İnsanüstü bir yeti midir bu?

Hayır.

Canlılığın evriminin seyrini incelemeye, toplumsal yapıların
gelişimlerinin izlerini sürmeye, kendini toplumdaki tipolojilerin
öznelerinin yerine koymaya, "bu, şu koşullarda nasıl bu oldu?"ya cevap
aramaya devam ettikçe, kesintisiz bir sürekliliğin adeta parçası haline
gelir insan. Anlamak sancısı, yerini yeni bir tür zihinsel lezzete
bırakıyordur. Baktığı her şey olağanlaşmaya başlar. Dupduru, kesintisiz
bir akış vardır gözlerinin önünde. Tarihin nehri.

Fabrika bacaları tüter, köyler ölür, yapı cepheleri dönüşür, intihar
vakaları artar, mısraların kurguları değişir, yenidoğan ölümleri azalır,
resim tekniği gelişir, işgaller başlar, kentler ölür, grevler patlar,
radyoaktivite keşfedilir, banliyölerden yeni müzik akımları, kent
meydanları dolar, kriz gümbürder, kemerler sıkılı, ormanlar kesilir,
dilde yeni kelimeler, çocuklar ağaç diker, mevzilerde obüs atışları,
devrim yükselir, karşı-devrim yükselir, cephelerde siperler dolar,
kantinlerde toplantılar alınır, şampiyonluk maçında beklenmedik
sloganlar, ittifaklar kurulur, kamera açıları değişir, gençler vurulur,
krallar padişahlar devrilir, kentler doğar, yapı cepheleri yine dönüşür,
şarkı sözleri dönüşür... Akışa bir defa kapıldıktan sonra, kafayı nereye
çevirirsek çevirelim, odağımıza aldığımız şeyleri akışın dışında
düşünememeye, görememeye başlarız. Çok katmanlı ilişkilenmeleri, üst
ölçekte anlamanın verdiği coşkuyla *"E elbette öyle, çok mantıklı, ya
nasıl olacaktı!"* nidaları yükselir iç seslerden.

Bu kavrayışa, maddenin ve zamanın belirli uğraklarından aldığımız dikine
kesitlerde, şeylerin geçmişten gelen köklerini ve geleceğe uzanan
dallarını anlamaya çalışmak eşlik eder. Yerini başka lezzetlere
bırakacak başka sancılar. Şeylerin ölümünü, ölümlerinin içindeki doğumu,
şeylerin doğumunu, doğumlarının içindeki ölümü ve bütün bu devinimdeki
yaşamlarını, değişimlerini, dönüşümlerini ve iç gerilimlerini
anlamak. Mistik bir sırra vakıf olmaya çalışmak değildir bu. Ufkumuz ve
aklımız yettiğince, bilimin metodolojisini kullanarak, değiştirebilmek
için anlamak çabasıdır.

Çocukluk nerede biter, yetişkinlik nerede başlar? Yetişkinlik, mümkün
müdür? İnsan doğası değişir mi? İnsanın değişen doğasının belirleyenleri
nelerdir? Dün feodalite nasıl bir toplumsal formasyondu? Bütün
coğrafyalarda, bütün tarihsel kesitlerde aynı mı seyretmişti? Bitmiş
miydi? İnancın, dinlerin gelişimini ve bireydeki yansımasını
açıklayabilir miyiz? Bugün kapitalizmde gelecek kaygısı olmadan, barış
içinde yaşanabilir mi? Sinema dizilerden intikam alabilir mi? Şiire
ihtiyacımız var mı? Devlet kimin için vardır? Adaletin toplumsal
değişkenleri nelerdir? Seçimlerden sonra meclis aritmetiği nasıl olacak?
Yeni dönemde emekçilerin geçim ve gelecek dertleri hafifleyecek midir?
Bir sevdiğimizin kaybı, aşılabilir mi? Nasıl? Yıkımları kolektif
belleğimizde nasıl yaşatacağız? Toplumların bağrındaki yaralar nasıl
sarılır? Kentler küllerinden doğabilirler mi? Devrim, gerçekçi mi?
Sorular geçişken, sorular sınırsız... Beşeri olandan toplumsal olana,
toplumsal olandan beşeri olana, grilerle dolu bir coğrafyada farklı
ölçeklerde, hissetmenin ağırlığıyla kuşatılmış, kesit almalı, bütünü ve
parçayı görmeli bir usavurum çabası. Zor iş!

Fakat, neticede, fark etmesi gökyüzünün rengine dair tartışmadaki kadar
kolay olmasa da her zaman; bir tarihsel/toplumsal konuya dair iki farklı
argüman varsa, bunlardan maddenin, doğanın ve tarihin içsel
bağıntılarını, diyalektik ilişkilenmelerini gözeteni, genellikle,
argümanların öne sürüldükleri bağlamda, o konuyu daha incelikli ve
kapsamlı bir biçimde açıklıyordur, maddeye hakkını daha iyi veriyordur.

Soyutlama, usavurma, kuramlaştırma, durağan değil süreğen bir çabadır,
eksiği gediği olabilir, geliştirilebilir, kaba ve ince ayarı
yapılabilir. Kuram tarihle sınanır. Onu tarttığımız terazide, karşı
kefede tarihin maddesi bulunur. Soyutlamamızın doğruluğu yanlışlığı,
maddi gerçeği açıklama kabiliyeti, yine bu maddeyle kıyaslanarak
belirlenir. Kuramımız, maddeye karşılık gelmiyorsa, maddeyi değil, ancak
kuramımızı değiştirebiliriz.

#### İdeolojinin belirlediği bir refleks
Tutarlı kuramsal çerçeveler çizmek, önünde sonunda, bir metodolojiyi ve
felsefeyi bilince çıkarmak ve refleks haline getirmekle, ilgili zihinsel
kas gruplarını geliştirmekle mümkündür. Basitçe söylersek, bir
bağlantılar kurabilmek ve çeşitli unsurlar arasında ağırlıklandırma
yapabilmek işidir. Tarihte olup bitmiş her bir olayı en ince
ayrıntılarına kadar bilmek veya öğrenmek değildir örneğin. Hatta en
büyük kuram hokkabazları böylelerinden, ortalığı en bilinmez kenarda
köşede kalmış bilgilerin bombardımanına tutup bilgiler arasındaki
bağlantıları el çabukluklarıyla değiştirenlerden, ya da her bilgi
parçasına aynı ya da yanlış ağırlığı verenlerden, esas oluşu ve taliliği
silikleştirenlerden çıkar.

Tarihin maddesine sorulacak soruların ve soyutlama sürecine konu edilen
problematiklerin önem hiyerarşisini, onların maddi yaşamdaki
karşılıklarının yakıcılıkları belirler. Peki kim için, ne kadar yakıcı?
Yakıcılık düzeyinin tespiti, dönemsel esas ve dönemsel talilerin
tanımlanmasına bağlıdır. Buradaki kerteriz, soyutlayan öznenin ideolojik
konumuna göre alınır. Yani toplumun maddesini kurama çıkarmak, bu tespit
işlemi, genelgeçer doğruların adlarının konması değil, iradi bir tercih
sürecidir de aynı zamanda. Neyi nasıl tespit ettiğimiz ideolojiye
dairdir, toplumun maddesine şekil verme amacını, "yalnızca yorumlamayı
değil, fakat değiştirmeyi" içsel olarak güder, yine öncülüğün
konusudur. Bunlara da geleceğiz.

Maddeyle muhabbetimize şimdilik ara verelim. Doğayı, maddeyi ve toplumu
türlü hileyle aldatmaya çalışan adi, piyasacı ve din taciri bir
iktidarın bize yaşattığı felaketin gölgesinde, herkes kadar yoğun bir
zihinsel ve ruhsal tükenmişliği yaşarken yazmaya çalıştım. Dilimiz
sürçtüyse affola. Umarım bu gevezelik okura bir şeyler ifade etmiştir.

Sosyalist öncülüğe dair fikir duvarımızın ilk sırasındaki maddecilik
tuğlalarının kimi temel unsurlarını tartışmaya çalıştık. Bir sonraki
blog yazısında, öncülük ve iktidar hedefi perspektifinden "Bilimsel
yöntemi benimsemek zorunda mıyız? Maddeci olmak zorunda mıyız?
Tarihselci olmak zorunda mıyız? Tarihsel maddeci olmak zorunda mıyız?"
gibi sorulara yanıt arayacak; akademik bilgi, ideoloji ve siyaset
arasında bir ayrıma gidip, ardından sıradan yurttaşa ve irtifalarda
kanat çırpan kuşlara geri döneceğiz.
