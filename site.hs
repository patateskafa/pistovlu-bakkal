--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
import           Data.Monoid ((<>))
import           Control.Monad (liftM)
import           Text.Pandoc.Highlighting (Style, zenburn, styleToCss)
import           Text.Pandoc.Options      (ReaderOptions (..), WriterOptions (..))
--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    match ("images/**" .||. "favicon.ico" .||. "fonts/*") $ do
        route   idRoute
        compile copyFileCompiler

    create ["css/syntax.css"] $ do
    route idRoute
    compile $ do
        makeItem $ styleToCss pandocCodeStyle

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match (fromList ["hakkinda.markdown", "iletisim.markdown"]) $ do
        route   $ setExtension "html"
        compile $ myPandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    -- Etiketçilik
    tags <- buildTags "posts/*" (fromCapture "tags/*/index.html")

    tagsRules tags $ \tag pattern -> do
        let title = "\"" ++ tag ++ "\"" ++ " etiketli yazılar"
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll pattern
            let ctx = constField "title" title
                      `mappend` listField "posts" postCtx (return posts)
                      `mappend` defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/tag.html" ctx
                >>= loadAndApplyTemplate "templates/default.html" ctx
                >>= relativizeUrls

    -- Sayfalama
    pag <- paginate "posts/*"
    paginateRules pag $ \pageNum pattern -> do
      route idRoute
      compile $ do
          posts <- recentFirst =<< loadAll pattern
          let paginateCtx = paginateContext pag pageNum
              ctx =
                  constField "title" ("Yazı Arşivi - Sayfa " ++ (show pageNum)) <>
                  listField "posts" (postCtxWithTags tags) (return posts) <>
                  paginateCtx <>
                  defaultContext
          makeItem ""
              >>= loadAndApplyTemplate "templates/postlar.html" ctx
              >>= loadAndApplyTemplate "templates/default.html" ctx
              >>= relativizeUrls

    match "posts/*" $ do
        route $ setExtension "html"
        compile $ myPandocCompiler
            >>= saveSnapshot "content"
            >>= loadAndApplyTemplate "templates/post.html"    (postCtxWithTags tags)
            >>= loadAndApplyTemplate "templates/default.html" (postCtxWithTags tags)
            >>= relativizeUrls

    match "index.html" $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "posts/*"
            let indexCtx =
                    listField "posts" (postCtxWithTags tags) (return $ take 6 posts) `mappend`
                    defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    
    -- RSS beslemesi
    create ["feed.xml"] $ do
      route idRoute
      compile $ do
        let feedCtx = postCtx `mappend` bodyField "description"
        posts <- fmap (take 10) . recentFirst =<<
          loadAllSnapshots "posts/*" "content"
        renderRss feedConfig feedCtx posts

    match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
pandocCodeStyle :: Style
pandocCodeStyle = zenburn

myPandocCompiler :: Compiler (Item String)
myPandocCompiler =
  pandocCompilerWith
    defaultHakyllReaderOptions
    defaultHakyllWriterOptions
      { writerHighlightStyle   = Just pandocCodeStyle
      }

--------------------------------------------------------------------------------
postCtx :: Hakyll.Context String
postCtx =
    dateField "date" "%e/%m/%Y" `mappend`
    defaultContext

postCtxWithTags :: Tags -> Hakyll.Context String
postCtxWithTags tags = tagsField "tags" tags `mappend` postCtx

--------------------------------------------------------------------------------
paginate :: (MonadMetadata m, MonadFail m) => Pattern -> m Paginate
paginate pattern = buildPaginateWith grouper pattern makeId

grouper :: (MonadMetadata m, MonadFail m) => [Identifier] -> m [[Identifier]]
grouper = liftM (paginateEvery 10) . sortRecentFirst

makeId :: PageNumber -> Identifier
makeId pageNum = fromFilePath $ if (pageNum == 1) then "arsiv.html" else show pageNum ++ "/arsiv.html"

feedConfig :: FeedConfiguration
feedConfig = FeedConfiguration
  { feedTitle = "Piştovlu Bakkal"
  , feedDescription = "Küçük ama yayılmacı hobi bahçesi"
  , feedAuthorName = "Alp Atamanalp"
  , feedAuthorEmail = "alp@pistovlubakkal.com"
  , feedRoot = "https://pistovlubakkal.com"
  }
